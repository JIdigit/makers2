from django.urls import path
from .views import *


urlpatterns = [
    path('', EmirTemplateView.as_view(), name='emir-home')
]
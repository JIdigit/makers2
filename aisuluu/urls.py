from django.urls import path
from .views import AisuluuTemplateView


urlpatterns = [
    path('', AisuluuTemplateView.as_view(), name='aisuluu'),
]